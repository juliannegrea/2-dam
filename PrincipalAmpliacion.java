package ampliacion;


import java.io.*;

public class PrincipalAmpliacion {

	public static void main(String[] args) {
		
		
		/*Ficheros 
		 * throw creas expeciones con una clase que hereda de una clase de Expepcion ya sea IOExcepcion*/
		/* Clase File es una clase estatica*/
		
		/*secuencial hay que pasar por todos los datos para encontrar uno 
		  secuecial ir directo al archivo 
		 		 texto con buffer sin buffer 
		 		 	buffer (reader)(write);
		 		 	
		 		 
		 		 binarios foto video audio 8bytes*/
		
		
			try {
				
				
				/*creamos una carpeta codigo etxto doc
				 * luego se puede crear una carpeta dentro de otra*/
				
				File fichero=new File("Prueba.txt");
				
				File codigo = new File("codigo");
				File texto = new File(codigo,"texto");
				File doc = new File("doc");

				
			
				fichero.createNewFile();
				codigo.mkdir();
				texto.mkdir();
				doc.mkdir();
				
				
				
				
				
				/*es una clase statica y no necesita objetos y solo llama a sus metodos segun nos interese en este caso a tempFile ficherotemporal */
				File.createTempFile("abc", "zzz");
				
				
				
				
				
				
			} catch (IOException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			
		
		
		
		
		

	}

}
