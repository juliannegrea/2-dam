#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>
#include<wait.h>


//fork() crea nuevos procesos sin nigun tipo de parametro.


int main(int argc, char const *argv[])
{
	
	pid_t pid;
	pid_t espera;
	int status; 					//estado en el que se encuentra el proceso.

	pid = fork();

		if (pid == 0) //se crea el hijo
		{
			printf("Soy hijo %d con padre %d \n", getpid(),getppid());
				printf("Yo hijo ejecuto mi proceso voy a terminar en 5 segundos\n");
					
					sleep(5); //esta durmiento y despues termina con exit();
				
						exit(0);
		}else

			if (pid>0) //padre es decir que no sea 0 ni -1 
			{
				printf("Soy padre %d con hijo %d y mi padre %d \n", getpid(), pid,getppid());
				printf("Yo padre ahora esta esperando al hijo durante 5 segundos hasta que acabe\n"); 
				//hay que crear una variable para guardar lo que devuelve wait
					espera=wait(&status);
					printf("Mi hijo %d acaba de terminar su proceso\n", espera);
			}else
				printf("Fallo del fork\n");




	return 0;
}